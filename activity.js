//  2. Create a new collection opf users


db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "email": "billgates@mail.com",
        "department": "Operations"
    },

    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "email": "janedoe@mail.com",
        "department": "HR"
    }
])




// 3. Find users with letter s in first name or d in last name
db.users.find(
    {
        $or: [
            {"firstName": {$regex: "s", $options: "$i"}},
            {"lastName": {$regex: "d", $options: "$i"}}
        ]
    },

    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1
    }
)

// 4. find users from HR department with age greater or equal to  70
db.users.find(
    {
        $and: [
            {"department": "HR"},
            {"age": {$gte: 70}}
        ]
    }
)

// 5. find users with e in name with age greater than or equal to 30
db.users.find(
    {
        $and: [
            {"firstName": {$regex: "e", $options: "$i"}},
            {"age": {$lte: 30}}
        ]
    }
)
